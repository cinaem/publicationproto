---
layout: archive
title: Repositories organization
excerpt: PP source code and repositories organization in Gitlab
tags: PP, evoting, opensource, AGPL, release, source code, git, gitlab, repository, repositories
permalink: /code/
image:
  feature: code-banner.png
---

Please find below the git repositories composing the ** platform source code and documentation. 

## Frontend
<div class="cards">

{% include repo.html
  url="protocol-core/chvote-protocol"
  title="Repository 1"
  teaser="1.png"
  excerpt="Set of backend applications running the core of the voting protocol. It includes the Control Components and the Bulletin Board applications."
%}

{% include repo.html
  url="protocol-core/chvote-protocol-model"
  title="Repository 2"
  teaser="2.png"
  excerpt="Defines the models for the CHVote protocol that are needed outside of the protocol itself."
%}

{% include repo.html
  url="protocol-core/chvote-protocol-client"
  title="Repository 3"
  teaser="3.png"
  excerpt="Angular library exposing the CHVote algrotihtms to be used by the voting client."
%}

</div>

## Backend

<div class="cards">

{% include repo.html
  url="protocol-core/chvote-protocol"
  title="Repository 1"
  teaser="1.png"
  excerpt="Set of backend applications running the core of the voting protocol. It includes the Control Components and the Bulletin Board applications."
%}

{% include repo.html
  url="protocol-core/chvote-protocol-model"
  title="Repository 2"
  teaser="2.png"
  excerpt="Defines the models for the CHVote protocol that are needed outside of the protocol itself."
%}

{% include repo.html
  url="protocol-core/chvote-protocol-client"
  title="Repository 3"
  teaser="3.png"
  excerpt="Angular library exposing the CHVote algrotihtms to be used by the voting client."
%}

</div>

## Client
<div class="cards">

{% include repo.html
  url="protocol-core/chvote-protocol"
  title="Repository 1"
  teaser="1.png"
  excerpt="Set of backend applications running the core of the voting protocol. It includes the Control Components and the Bulletin Board applications."
%}

{% include repo.html
  url="protocol-core/chvote-protocol-model"
  title="Repository 2"
  teaser="2.png"
  excerpt="Defines the models for the CHVote protocol that are needed outside of the protocol itself."
%}

{% include repo.html
  url="protocol-core/chvote-protocol-client"
  title="Repository 3"
  teaser="3.png"
  excerpt="Angular library exposing the CHVote algrotihtms to be used by the voting client."
%}

</div>

## Documentation
<div class="cards">

{% include repo.html
  url="protocol-core/chvote-protocol"
  title="Repository 1"
  teaser="1.png"
  excerpt="Set of backend applications running the core of the voting protocol. It includes the Control Components and the Bulletin Board applications."
%}

{% include repo.html
  url="protocol-core/chvote-protocol-model"
  title="Repository 2"
  teaser="2.png"
  excerpt="Defines the models for the CHVote protocol that are needed outside of the protocol itself."
%}

{% include repo.html
  url="protocol-core/chvote-protocol-client"
  title="Repository 3"
  teaser="3.png"
  excerpt="Angular library exposing the CHVote algrotihtms to be used by the voting client."
%}

</div>

## Docker
<div class="cards">

{% include repo.html
  url="documentation/chvote-docs"
  title="Docker Images"
  teaser="docker.png"
  excerpt="Common technical documentation for the CHVote projects."
%}

</div>

## Website
<div class="cards">

{% include repo.html
  url="infra"
  title="Website"
  teaser="website.png"
  excerpt="Images used to build and run the applications."
%}

</div>